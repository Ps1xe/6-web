//Не забудьте подключить CSS-файл к компоненту
//https://monsterlessons.com/project/lessons/react-css-komponentyj-podhod

import { Card } from "../Card/Card";
import './Column.css'

/**
 * Обратите внимание на структуру нашего проекта
 */

/**
 * Компонент колонки. Должен принимать пропсы name, cards, isDisabled
 * Мы должны отображать name как имя колонки;
 * Мы должны отображать все карточки в колонке (cards);
 * Если передан пропс isCardsHidden мы должны отображать только имя колонки без карточек;
 * isCardsHidden по дефолту должен быть со значением false 
 */
export function Column({name, cards, isCardsHidden}) {

  const renderCards = () => {
    /**
     * Метод рендеринга карточек -- это хорошая практика, выносить сложный рендеринг в отдельную функцию
     * Учтите, что каждая карточка должна иметь уникальный key
     * https://ru.reactjs.org/docs/lists-and-keys.html
     */

    if (isCardsHidden || !cards) {
      return null;
    }

    return cards.map((card) => {
      return <Card name={card.name} key={card.id}/>
    });

  };

  return <div className="column"> <span>{name}</span> {renderCards()}</div>;
}
