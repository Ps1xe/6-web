//Не забудьте подключить CSS-файл к компоненту
//https://monsterlessons.com/project/lessons/react-css-komponentyj-podhod
import './Card.css'

/**
 * Компонент карточки. Должен принимать пропсы id, name
 * Мы должны отображать name как имя карточки.
 */

export function Card({name}) {
  return <div className = "card"><span>{name}</span></div>;
}
